#include <stdint.h>

typedef struct {
    uint8_t * buffer;
    uint8_t head;
    uint8_t tail;
    uint8_t maxLen;
} circBuf_t;

// It starts the buffer with no data
void circBufInit(circBuf_t *c, uint8_t *buffer, uint8_t maxLen);

// Returns
//    0 if the data was added succesfully
//    -1 if the buffer is full.
int circBufPush(circBuf_t *c, uint8_t data);

// Returns
//      The content of the buffer
//      -1 if the buffer is empty.
int circBufPop(circBuf_t *c);
