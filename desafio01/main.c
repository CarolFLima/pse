#include <stdio.h>
#include <time.h>
#include <string.h>
#include "circBuffer.h"

#define MAXLEN 20 // 80 bytes over 4 bytes

void main () {

  circBuf_t cb;
  uint8_t buffer[MAXLEN];

  circBufInit(&cb, &buffer, MAXLEN);

  for (int i = 0; i < MAXLEN; i++){
    int r = rand() % 255;
    circBufPush(&cb, r);
  }

  uint8_t data[MAXLEN];
  memcpy(data, cb.buffer, MAXLEN);

  for (int i = 0; i < MAXLEN; i++){
    //      The following line was replaced in order to test the circBufPop function,
    //      even though it's also a way of comparing the buffer with the copied data
    // printf("Line %d - Copied data: %d, Buffer: %d\n", i+1, data[i], cb.buffer[i]);

    printf("Line %d - Copied data: %x, Buffer: %x\n", i+1, data[i], circBufPop(&cb));
  }

}
