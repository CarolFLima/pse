#include "circBuffer.h"


void circBufInit(circBuf_t *c, uint8_t *buffer, uint8_t maxLen) {
    c->buffer = buffer;
    c->maxLen = maxLen;
    c->head = 0;
    c->tail = 0;
}

int circBufPush(circBuf_t *c, uint8_t data) {
    int next = c->head + 1;
    if (next >= c->maxLen)
        next = 0;

    if (next == c->tail)
        return -1;

    c->buffer[c->head] = data;
    c->head = next;
    return 0;
}

int circBufPop(circBuf_t *c) {
    if (c->head == c->tail)
        return -1;

    int next = c->tail + 1;

    if(next == c->maxLen){
        next = 0;
    }
    
    if(next == c->maxLen-1){
        next = c->maxLen;
        c->head = next + 1;
    }


    int data = c->buffer[c->tail];
    c->tail = next;

    return data;
}
