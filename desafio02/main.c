#include <stdio.h>
#include <math.h>
#include <time.h>
#include <stdlib.h>

//These 2 functions are for demonstration purposes only
void send(int a){
}

int getData(){
  return (rand() % 180);

}

int main()
{
    clock_t average1_t;
    for(int j = 0; j < 1000; j++){

        clock_t start_t, end_t, total_t;
        start_t = clock();

        int data[8] = {0};
        int counter = 0;
        int accumulator = 0;
        int average = 0;

        for(int i=0; i<800; i++){
            int r =  getData();
            data[i%8] = cos(r)*r + r*r;
            accumulator += cos(r)*r + r*r;
            counter++;

            average = accumulator / counter;
            send(average);
        }

        end_t = clock();
        total_t = (end_t - start_t);
        average1_t += total_t;
    }

    printf("Total number of clock cycles: %ld\n", average1_t/1000);

    // Improved code
    clock_t average2_t;
    int r2 = 0;

    for(int j = 0; j < 1000; j++){
        clock_t start_t, end_t, total_t;
        start_t = clock();

        int data[8] = {0};
        int counter = 0;
        int accumulator = 0;
        int average = 0;

        for(int i=0; i<800; i++){
            int r = getData();
            r2 = r*r;
            data[i&7] = r*((1 + (r2>>1) + ((r2*r2)>>5)) + r);
            accumulator += data[i&7];
            counter++;

            if(i&7 == 7){
               average = accumulator / counter;
               send(average);
            }
        }

        end_t = clock();
        total_t = (end_t - start_t);
        average2_t += total_t;
    }

    printf("Total number of clock cycles: %ld\n", average2_t/1000);

    printf("Improvement of: %lf\n", (((float) average1_t - average2_t)/(float)average1_t) * 100);

    return 0;
}
